<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="<?php echo $keywords ?>">
    <title><?php echo $title ?></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <link rel="stylesheet" href="scss/global.css">
</head>

<body>
    
    <?php
        // 2. Perform database query
        $result = db_select("subjects", "visible", 1, "ASC");
    ?>
    
    <header>
        <div class="row">
            <div class="container-fluid">
                <div class="logo">
                    <h1><?php echo ucwords($lb); ?></h1>
                </div>
                <nav class="navbar pull-right">
                    <div class="navbar-list">
                        <?php
                            // 3. Use returned data (if any)
                            while($subject = mysqli_fetch_assoc($result)) {
                            // output data from each row
                        ?>
                            <div class="navbar-item"><a href="?p=<?php echo $subject['menu_name']; ?>"><?php echo ucfirst($subject['menu_name']); ?></a></div>
                        <?php
                            }
                        ?>
                        <?php
                            // 4. Release returned data
                            mysqli_free_result($result);
                        ?>
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <main>