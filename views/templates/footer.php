    </main>
    <footer>
        <div class="row red-bg">
            <div class="container">
                <div class="slogan-block">
                    <h3 class="slogan">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="container">
                <div class="col-6 copyright-block">
                    <div class="copyright">
                        <p>lovebecause.org&copy;</p>
                    </div>
                </div>
                <div class="col-6 social-links-block">
                    <div class="social-links">
                        <a href="#"><img src="http://iconbug.com/download/size/512/icon/481/small-blue-facebook/"></a>
                        <a href="#"><img src="http://iconbug.com/download/size/512/icon/5537/twitter-circle/"></a>
                        <a href="#"><img src="http://www.airedaleenterprise.org.uk/wp-content/uploads/2012/07/google-plus-icon.png"></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script src="js/script.js"></script>
</body>
</html>