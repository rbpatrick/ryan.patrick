<?php
    // 2. Perform database query
    $result = db_select("pages", "subject_id", 1, "ASC");
?>

<?php
    // 3. Use returned data (if any)
    while($page = mysqli_fetch_assoc($result)) {
        // output data from each row
        echo $page['content'];
    }
?>

<?php
    // 4. Release returned data
    mysqli_free_result($result);
?>





<!--<div class="row welcome-img dark-bg-img">
    <div class="container">
        <div class="home-content-block">
            <div class="center Absolute-Center">
                <h1>We <span style="color: red;">Love Because</span> Christ<br>First Loved Us</h1>
            </div>
        </div>
    </div>
</div>
<div id="home-bg-events" class="row solid-color">
    <div class="container">
        <div class="home-content-block">
            <div class="col-12">
                <div class="row">
                    <div class="block-title">
                        <h1>Events</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="block-info">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet malesuada feugiat. Curabitur fermentum bibendum nulla, non dictum ipsum tincidunt non. Quisque convallis pharetra tempor. Donec id pretium leo. Pellentesque luctus massa non elit viverra pellentesque. Cras vitae neque molestie, rhoncus ipsum sit amet, lobortis dui. Fusce in urna sem. Vivamus vehicula dignissim augue et scelerisque. Etiam quam nisi, molestie ac dolor in, tincidunt tincidunt arcu.</p>
                        <div class="center"></div>
                        <a href="?p=events" class="center"><div class="learn-more-btn">Learn More</div></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="home-bg-media" class="row dark-bg-img">
    <div class="container">
        <div class="home-content-block">
            <div class="col-12">
                <div class="row">
                    <div class="block-title">
                        <h1>Media</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="block-info">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet malesuada feugiat. Curabitur fermentum bibendum nulla, non dictum ipsum tincidunt non. Quisque convallis pharetra tempor. Donec id pretium leo. Pellentesque luctus massa non elit viverra pellentesque. Cras vitae neque molestie, rhoncus ipsum sit amet, lobortis dui. Fusce in urna sem. Vivamus vehicula dignissim augue et scelerisque. Etiam quam nisi, molestie ac dolor in, tincidunt tincidunt arcu.</p>
                        <a href="?p=media"><div class="learn-more-btn">Learn More</div></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="home-bg-contact" class="row solid-color">
    <div class="container">
        <div class="home-content-block">
            <div class="col-12">
                <div class="row">
                    <div class="block-title">
                        <h1>Contact</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="block-info">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet malesuada feugiat. Curabitur fermentum bibendum nulla, non dictum ipsum tincidunt non. Quisque convallis pharetra tempor. Donec id pretium leo. Pellentesque luctus massa non elit viverra pellentesque. Cras vitae neque molestie, rhoncus ipsum sit amet, lobortis dui. Fusce in urna sem. Vivamus vehicula dignissim augue et scelerisque. Etiam quam nisi, molestie ac dolor in, tincidunt tincidunt arcu.</p>
                        <a href="?p=contact"><div class="learn-more-btn">Learn More</div></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="home-bg-about" class="row dark-bg-img">
    <div class="container">
        <div class="home-content-block">
            <div class="col-12">
                <div class="row">
                    <div class="block-title">
                        <h1>About</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="block-info">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet malesuada feugiat. Curabitur fermentum bibendum nulla, non dictum ipsum tincidunt non. Quisque convallis pharetra tempor. Donec id pretium leo. Pellentesque luctus massa non elit viverra pellentesque. Cras vitae neque molestie, rhoncus ipsum sit amet, lobortis dui. Fusce in urna sem. Vivamus vehicula dignissim augue et scelerisque. Etiam quam nisi, molestie ac dolor in, tincidunt tincidunt arcu.</p>
                        <a href="?p=about"><div class="learn-more-btn">Learn More</div></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>-->