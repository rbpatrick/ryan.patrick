<?php
    /**
     * Render Template
     *
     * @param array $data
     */
    function render($template, $data = array()) {
        $path = "../views/" . $template . ".php";
        if (file_exists($path)) {
            foreach ($data as $key => &$value) {
                $value = htmlspecialchars($value);
            }
            extract($data);
            require($path);
        }
    }

    /* Redirect to a different page */
    function redirect_to($location) {
        header("Location: " . $location);
        exit;
    }
?>