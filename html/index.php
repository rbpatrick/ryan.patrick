<?php
    ini_set('display_errors', 1);
    // 1. Create database connection
    require_once("../includes/db_connection.php");
    // Grab my PHP functions
    require_once('../includes/functions.php');    

    // Determine which page to render 
    if (isset($_GET['p'])) {
        $page = $_GET['p'];
    } else {
        $page = 'home';
    }

    // Render page 
    switch($page) {
        case 'home':
            render('templates/header', array(
                'title'         =>  'Love Because | Home',
                'lb'            =>  'Love Because',
                'page_title'    =>  'Home',
                'keywords'      =>  'love, because, lovebecause, cookeville, tn, website'
            ));
            render('home');
            render('templates/footer');
            break;

        case 'events':
            render('templates/header', array(
                'title'         =>  'Love Because | Events',
                'lb'            =>  'Love Because',
                'page_title'    =>  'Events',
                'keywords'      =>  'love, because, lovebecause, events, news'
            ));
            render('events');
            render('templates/footer');
            break;

        case 'media':
            render('templates/header', array(
                'title'         =>  'Love Because | Media',
                'lb'            =>  'Love Because',
                'page_title'    =>  'Media',
                'keywords'      =>  'love, because, lovebecause, media, videos, recap'
            ));
            render('media');
            render('templates/footer');
            break;

        case 'contact':
            render('templates/header', array(
                'title'         =>  'Love Because | Contact',
                'lb'            =>  'Love Because',
                'page_title'    =>  'Contact',
                'keywords'      =>  'love, because, lovebecause, contact, email, phone, number'
            ));
            render('contact');
            render('templates/footer');
            break;

        case 'about':
            render('templates/header', array(
                'title'         =>  'Love Because | About',
                'lb'            =>  'Love Because',
                'page_title'    =>  'About',
                'keywords'      =>  'love, because, lovebecause, about, josh, valintine'
            ));
            render('about');
            render('templates/footer');
            break;
    }

    // End database connection 
    if (isset($db)) {
        mysqli_close($db);
    }

?>